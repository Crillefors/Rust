extern crate rand;


use std::io;
use std::cmp::Ordering;
use rand::Rng;

fn main() {
    
    println!("Guess the number!");

    let secret_number = rand::thread_rng().gen_range(1, 101);

    //println!("The secret number is: {}", secret_number);
    
    let mut total_guess = 0;

    loop {
	    total_guess += 1;
        println!("Please input your guess.");

        let mut guess = String::new();

        io::stdin().read_line(&mut guess)
            .expect("Failed to read line");

        
        let guess: u32 = match guess.trim().parse() {
            Ok(num) => num,
            Err(_) => {println!("What kind of number is {} \nTry again...\n\n",guess);
                continue;
            }

        };

        println!("You guessed: {}", guess);

        match guess.cmp(&secret_number) {
            Ordering::Less    => println!("Too small!"),
            Ordering::Greater => println!("Too big!"),
            Ordering::Equal   => {
                println!("You win! it took you {} guess(es)",total_guess);
                break;
            }
        }
    }
}
